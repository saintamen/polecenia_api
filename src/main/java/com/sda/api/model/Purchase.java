package com.sda.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private double quantity;
    private String note;
    private String location;

    @JsonIgnore
    @ManyToOne()
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private AppUser owner;

    public Purchase(PurchaseDto purchaseDto) {
        this.name = purchaseDto.getName();
        this.quantity = purchaseDto.getQuantity();
        this.note = purchaseDto.getNote();
        this.location = purchaseDto.getLocation();
    }
}

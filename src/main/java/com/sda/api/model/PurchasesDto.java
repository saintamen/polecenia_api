package com.sda.api.model;

import com.sda.api.model.Purchase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PurchasesDto {
    private String owner;
    private List<Purchase> purchaseList;
}

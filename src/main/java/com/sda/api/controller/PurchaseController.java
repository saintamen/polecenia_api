
package com.sda.api.controller;
import com.sda.api.model.Purchase;
import com.sda.api.model.PurchaseDto;
import com.sda.api.model.PurchasesDto;
import com.sda.api.services.PurchaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/purchase/")
@RequiredArgsConstructor
public class PurchaseController {
    private final PurchaseService purchaseService;

    @GetMapping("/{username}")
    @ResponseStatus(HttpStatus.OK)
    public PurchasesDto getAllForUser(@PathVariable("username") String username) {
        return purchaseService.getAll(username);
    }

    @PutMapping("/{username}")
    @ResponseStatus(HttpStatus.CREATED)
    public void put(@PathVariable("username") String username, @RequestBody PurchaseDto purchase) {
        purchaseService.put(username, purchase);
    }

    @DeleteMapping("/{username}/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void remove(@PathVariable("username") String username, @PathVariable("id") Long id){
        purchaseService.remove(username, id);
    }
}

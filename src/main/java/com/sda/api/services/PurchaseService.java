package com.sda.api.services;

import com.sda.api.model.Purchase;
import com.sda.api.model.PurchaseDto;
import com.sda.api.model.PurchasesDto;
import com.sda.api.model.AppUser;
import com.sda.api.repository.AppUserRepository;
import com.sda.api.repository.PurchaseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PurchaseService {
    private final PurchaseRepository purchaseRepository;
    private final AppUserRepository appUserRepository;

    public PurchasesDto getAll(String username) {
        AppUser appUser = getOrCreateAppUser(username);
        return new PurchasesDto(appUser.getName(), new ArrayList<>(appUser.getPurchases()));
    }

    private AppUser getOrCreateAppUser(String username) {
        Optional<AppUser> appUserOptional = appUserRepository.findByName(username);
        AppUser appUser;
        if (!appUserOptional.isPresent()) {
            appUser = new AppUser(null, username, new HashSet<>());
            appUser = appUserRepository.save(appUser);
        } else {
            appUser = appUserOptional.get();
        }
        return appUser;
    }

    public void put(String username, PurchaseDto purchaseDto) {
        AppUser appUser = getOrCreateAppUser(username);

        Purchase newPurchase = new Purchase(purchaseDto);
        newPurchase.setOwner(appUser);

        purchaseRepository.save(newPurchase);
    }

    public void remove(String username, Long purchaseId) {
        AppUser appUser = getOrCreateAppUser(username);

        Optional<Purchase> purchaseOptional = appUser.getPurchases().stream().filter(purchase -> purchase.getId().equals(purchaseId)).findFirst();
        if (purchaseOptional.isPresent()) {
            Purchase purchase = purchaseOptional.get();
            purchase.setOwner(null);
            appUser.getPurchases().remove(purchase);
            purchaseRepository.save(purchase);
            purchaseRepository.delete(purchase);
        }

        appUserRepository.save(appUser);
    }


}

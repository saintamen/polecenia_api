Należy stworzyć aplikację do "obsługi" API dostępnego pod:<br/>
baseurl: http://194.181.116.187:20200/purchase<br/>
documentation: http://194.181.116.187:20200/documentation/swagger-ui.html<br/>

Aplikacja powinna pracować w pętli dopóki użytkownik nie wpisze "quit".<br/> 
Na początku pytamy użytkownika o jego "login". Po wpisaniu pobieramy jego profil z API. W profilu zapisana jest lista zakupów.<br/> 
Wyświetl użytkownikowi listę zakupów z profilu.<br/>


Po wyświetleniu listy zapytaj "co chce zrobić?". Użytkownik ma opcję:<br/>
- dodać produkt [z listy] <br/>
- usunąć produkt [z listy]<br/>
- ponownie pobrać i wypisać listę<br/>
Jeśli użytkownik wybierze dodanie produktu, to poproś go o DWA NIEZBĘDNE POLA, TJ. nazwę i ilość produktu.<br/> 
Następnie zapytaj go o pozostałe pola (skąd/z jakiego sklepu + notatka). Po wypełnieniu obiektu (PurchaseDto) <br/>
możesz wysłać go na serwer. Wysłanie wygląda następująco:<br/>

ObjectMapper mapper = new ObjectMapper();<br/>

PurchaseDto dto = new PurchaseDto(...);<br/>

String json = mapper.writeValueAsString(dto);<br/>

HttpRequest request = HttpRequest.newBuilder()<br/>
                .POST(HttpRequest.BodyPublishers.ofString(json))<br/>
                .uri(URI.create("http://194.181.116.187:20200/purchase/{username}"))<br/>
                .header("Content-Type", "application/json")<br/>
                .build()<br/>

... // dalej klasyczna implementacja wysyłania.<br/>

UWAGA! Zapytanie wstawiające nie zwraca odpowiedzi. Nie musimy go parsować!<br/>

Przy usuwaniu rekordu, pamiętaj że URL składa się z<br/> 

http://194.181.116.187:20200/purchase/{NAZWA_UZYTKOWNIKA}/{IDENTYFIKATOR}<br/>

gdzie {IDENTYFIKATOR} to id produktu który chcemy usunąć!<br/>
